package src.a06.e1;

import java.io.InputStream;
import java.util.List;
import java.util.function.BinaryOperator;

public class SequenceHelpersImpl implements SequenceHelpers {

    public SequenceHelpersImpl() {
    }

    @Override
    public <X> Sequence<X> of(X x) {
        return new Sequence<X>(){
            @Override
            public X nextElement() {
                return x;
            }
        };
    }
    
    @Override
    public <X> Sequence<X> cyclic(List<X> l) {
        return new Sequence<X>() {
            private int i = 0;
            @Override
            public X nextElement() {
                if(i == l.size()) {
                    i = 0;
                }
                i++;
                return l.get(i-1);
            }
            
        };
    }

    @Override
    public Sequence<Integer> incrementing(int start, int increment) {
        return new Sequence<Integer>() {
            private int curr = start;
            @Override
            public Integer nextElement() {
                curr += increment;
                int res = curr - increment;
                return res;
            }
        };
    }

    @Override
    public <X> Sequence<X> accumulating(Sequence<X> input, BinaryOperator<X> op) {
        return new Sequence<X>() {

            int i = 0;
            X next;
            
            @Override
            public X nextElement() { 
                if(i == 0) {
                    i++;
                    next = input.nextElement();
                    return next;
                }            
                i++;     
                next = op.apply(next, input.nextElement());
                return next;
            }
            
        };
    }

    @Override
    public <X> Sequence<Pair<X, Integer>> zip(Sequence<X> input) {
        return new Sequence <Pair<X, Integer>>(){
            private int i = 0;
            @Override
            public Pair<X, Integer> nextElement() {
                return new Pair<X, Integer>(input.nextElement(), i++);
            }
        };
    }
}
